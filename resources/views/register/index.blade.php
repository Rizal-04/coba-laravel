@extends ('layout.main')

@section ('container')

<div class="row justify-content-center  p-5 mt-5 ">
  <div class="col-md-5">
    <main>
    <h5 >Silahkan Masukan Nama, Email dan Password</h5>
      <form action="/register" method="post" class="p-3">
      @csrf
      <div class="mb-3 form-floating">
          <input name="name" id="name" type="text" class="form-control">
          <label for="name"  class="form-label">Nama</label>
        </div>
        <div class="mb-3 form-floating">
          <input name="email" id="email" type="email" class="form-control">
          <label  for="email" class="form-label">Email address</label>

        </div>
        <div class="mb-3 form-floating">
          <input name="password" id="password"  type="password" class="form-control">
          <label for="password" class="form-label">Password</label>

        </div>
        <div class="row">
        <button type="submit" class="btn btn-primary">Submit</button>
        <small class=" mt-3">sudah punya akun ? <a href="/login">Login</a></small>

        </div>
      
      </form>
    </main>
  </div>
</div>

@endsection