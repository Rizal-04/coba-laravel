@extends('dashboard.layouts.main')

@section('container')

<div
    class="flex flex-col"
    style=" max-width: 448px; width: 100%"
  >
  @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert" style="max-width:400px; width:100%">
      {{ session('success')}}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div
      style="width: 100%; padding: 0px; display: flex; background-color: white; flex-direction: column; align-items: center; margin-top: 60px;"
    >
      <div
        style="display: flex; justify-content: space-around; align-items: center; flex-direction: row; height: 150px; width: 100%;"
      >
        <div
          style="display: flex; justify-content: center; align-items: center;"
        >
          <img
            src="https://png.pngtree.com/element_our/png/20181206/users-vector-icon-png_260862.jpg"
            style="max-width: 80px; height: 80px; border-radius: 50%;"
          />
        </div>
        <div class="text-black" style="width: 70%;">
          <p>{{auth()->user()->name}}</p>
          <p>{{auth()->user()->email}}</p>
        </div>
      </div>
      <div>
      <a href="/update">
        <button type="submit">Update profile</button>
        </a>
      </div>
    </div>

  </div>

@endsection