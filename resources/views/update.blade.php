@extends('dashboard.layouts.main')

@section('container')

<div
    class="flex flex-col"
    style=" max-width: 448px; width: 100%"
  >
  <h2>Edit profil</h2>
    <div
      style="width: 100%; padding: 0px; display: flex; background-color: white; flex-direction: column; align-items: center; margin-top: 60px;"
    >
      <div
        style="display: flex; justify-content: space-around; align-items: center; flex-direction: row; height: 150px; width: 100%;"
      >
      <form action="/update" method="post" >
         @csrf

        @method('PUT')

        <div class="mb-3 form-floating">
          <input name="name" id="name" type="text" class="form-control" value="{{$user->name}}">
          <label for="name"  class="form-label">Nama</label>
        </div>
        <div class="mb-3 form-floating">
          <input name="email" id="email" type="email" class="form-control" value="{{$user->email}}">
          <label  for="email" class="form-label">Email</label>

        </div>
        <button type="submit">edit</button>
        </form> 
      </div>
      <div>
    
      </div>
    </div>

  </div>

@endsection