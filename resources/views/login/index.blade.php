@extends ('layout.main')

@section ('container')

<div class="row justify-content-center p-5 mt-5 ">
  <div class="col-md-5">
  @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert" style="max-width:400px; width:100%">
      {{ session('success')}}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if(session()->has('loginError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="max-width:400px; width:100%">
      {{ session('loginError')}}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <h5 >Silahkan Masukan Email dan Password untuk masuk ke halaman selanjutnya</h5>
    <main class="py-5">
    <form action="login" method="post" >
     @csrf
     <div class="mb-3 form-floating">
          <input name="email" id="email" type="email" class="form-control">
          <label  for="email" class="form-label">Email address</label>

        </div>
        <div class="mb-3 form-floating">
          <input name="password" id="password"  type="password" class="form-control">
          <label for="password" class="form-label">Password</label>

        </div>
        <div class="row">
        <button type="submit" class="btn btn-primary">Submit</button>
        <small class=" mt-3">belum punya akun ? <a href="/register">Register sekarang</a></small>
        </div>
      </form>
    </main> 
  </div>
</div>

@endsection