<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index () {
        return view('register.index', [
            'title' => 'Register',
            'active' => 'register'

        ]);
       }
       public function store(Request $request)
       {

          
            $validator = Validator::make($request->all(), [
               'name' => 'required',
               'email' => 'required|email:dns',
               'password' => 'required',
           ]);
   
           if ($validator->fails()) {
               return response()->json([
                   'success' => false,
                   'message' => 'Ada kesalahan',
                   'data' => $validator->errors()
               ]);
           }

           
           $hashedPassword = Hash::make($request->password);

           $input = $request->all();
           $input['password'] =  $hashedPassword ;
           $user = User::create($input);
   
           $success['token'] = $user->createToken('auth_token')->plainTextToken;
           $success['name'] = $user->name;
           $request->session()->flash('success', 'Registrasi successfull!! Please Login');

           // Redirect
           return redirect('/login');
  }

}