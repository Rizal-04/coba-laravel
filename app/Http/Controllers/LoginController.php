<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use Validator;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
   public function index () {
    return view('login.index', [
        'title' => 'Login',
        'active' => 'login'
    ]);
   }
   /**
    * @param App\Models\Post

    */

   public function auth(Request $request){

    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
        $auth = Auth::user();
            $success['token'] = $auth->createToken('auth_token')->plainTextToken;
            $success['name'] = $auth->name;
            $success['email'] = $auth->email;

            // response()->json([
            //     'success' => true,
            //     'message' => 'Login sukses',
            //     'data' => $success
            // ]);

            // $request->session()->flash('success', 'Login successfull!!');

            // Redirect
            return redirect('/dashboard');
    }
    else{
        return response()->json([
            'success' => false,
            'message' => 'Cek email dan password lagi',
            'data' => null
        ]);
    }
        
    
    }

   
    public function logout(Request $request){
        
    Auth::logout();
 
    $request->session()->invalidate();
 
    $request->session()->regenerateToken();
 
    return redirect('/');
    }
    public function getupdate(Request $request){
        return view('update')->with('user', auth()->user());
   
    }
    public function update(Request $request){

      $request->validate([
            'email' => ['required', 'email'],
            'name' => ['required'],
        ]);

        $user = auth()->user();

        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        // $validatedData['user_id'] = auth()->user()->id;

        // Post::where('id', $request->id)
        //     ->update($validatedData);
        session()->flash('success', 'Berhasil Edit Profil');
            return redirect('/profil');
            // ->with('success', 'Berhasil Edit Profil');
   
    }
}
